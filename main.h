#ifndef _MAIN_H_
#define _MAIN_H_

#include <stdio.h>
#include <malloc.h>

#ifndef M_PI
#define M_PI acos(-1)
#endif

#include "bmp_enums.h"
#include "bmp_io.h"
#include "bmp_image.h"
#include "sepia.h"
#include "timer.h"

#endif
