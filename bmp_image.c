#include "bmp_image.h"

struct pixel get_pixel( const struct image* image, uint64_t x, uint64_t y ) {
	return image->data[y * image->width + x];
}

void set_pixel( const struct image* image, uint64_t x, uint64_t y, struct pixel pixel ) {
	image->data[y * image->width + x ] = pixel;
}

uint64_t calculate_offset( uint64_t width ) {
	uint64_t offset = 4 - ( ( width * sizeof( struct pixel ) ) % 4 );
	return offset != 4 ? offset : 0;
}


struct bmp_header create_bmp_header( struct image* image ) {
	struct bmp_header header;
	uint32_t width = image->width;
	uint32_t height = image->height;
	size_t offset = calculate_offset( width );

	header.bfType = 0x4D42;
	header.bfileSize = ( width + offset ) * height + sizeof( struct bmp_header );
	header.bfReserved = 0;
	header.bOffBits = 54;
	header.biSize = 40;
	header.biWidth = width;
	header.biHeight = height;
	header.biPlanes = 1;
	header.biBitCount = 24;
	header.biCompression = 0;
	header.biSizeImage = height * ( width * sizeof( struct pixel ) + offset );
	header.biXPelsPerMeter = 0;
	header.biYPelsPerMeter = 0;
	header.biClrUsed = 0;
	header.biClrImportant = 0;

	return header;
}
