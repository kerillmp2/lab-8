#include "sepia.h"

extern void sepia_asm( float source[12], float result[12] );

void sepia_image_asm( struct image* source, struct image* target ) {
	struct pixel pixel;
	size_t image_size = source->width * source->height;
	size_t pixel_max = ( image_size / 4 ) * 4;
	size_t rest = image_size - pixel_max;
	size_t i, j;
	float old_p[12];
	float new_p[12];

	target->width = source->width;
	target->height = source->height;
	target->data = source->data;

	for( i = 0; i < pixel_max; i += 4 ) {
		for( j = 0; j < 4; j++ ) {
			pixel = source->data[i + j];
			old_p[j * 3] = pixel.r;
			old_p[j * 3 + 1] = pixel.g;
			old_p[j * 3 + 2] = pixel.b;
		}

		sepia_asm( old_p, new_p );
		for( j = 0; j < 4; j++ ) {
			pixel.r = new_p[j * 3];
			pixel.g = new_p[j * 3 + 1];
			pixel.b = new_p[j * 3 + 2];
			target->data[i + j] = pixel;
		}
	}

	for( i = 0; i < rest; i++ ) {
		pixel = source->data[i + pixel_max];
		target->data[i + pixel_max] = sepia_pixel( pixel );
	}
}

void sepia_image( struct image* source, struct image* result ) {
	uint32_t i, j;
	result->width = source->width;
	result->height = source->height;
	result->data = malloc( source->width * source->height * sizeof( struct pixel ) );

	for( i = 0; i < source->height; i++ ) {
		printf( "pixel_row: %d\n", i );
		for( j = 0; j < source->width; j++ ) {
			printf( "pixel_column: %d\n", j );
			set_pixel( result, i, j, sepia_pixel( get_pixel( source, i, j ) ) );
		}
		printf( "row %d finished\n", i );
	}
}

struct pixel sepia_pixel( struct pixel const pixel ) {
	static const float sep[3][3] = {
		{.393f, .769f, .189f},
            	{.349f, .686f, .168f},
            	{.272f, .543f, .131f}
	};
	struct pixel result_pixel;
	result_pixel.r = check( pixel.r * sep[0][0] + pixel.g * sep[0][1] + pixel.b * sep[0][2] );
	result_pixel.g = check( pixel.r * sep[1][0] + pixel.g * sep[1][1] + pixel.b * sep[1][2] );
	result_pixel.b = check( pixel.r * sep[2][0] + pixel.g * sep[2][1] + pixel.b * sep[2][2] );

	/*puts("sepia_pixel works");*/
	return result_pixel;
}

unsigned char check( uint64_t x ) {
	printf("%ld\n", x);
	if ( x < 256 ) {
		return x;
	}
	return 255;
}
