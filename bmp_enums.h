#ifndef _BMP_ENUMS_H_
#define _BMP_ENUMS_H_

enum read_status {
	READ_OK = 0,
	READ_HEADER_ERROR,
	READ_ERROR
};

enum write_status {
	WRITE_OK = 0,
	WRITE_ERROR
};

#endif
