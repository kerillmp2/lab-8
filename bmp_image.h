#ifndef _BMP_IMAGE_H_
#define _BMP_IMAGE_H_

#include <stdint.h>
#include <stdio.h>
#include <malloc.h>

struct __attribute__(( packed )) bmp_header {
	uint16_t bfType;
	uint32_t bfileSize;
	uint32_t bfReserved;
	uint32_t bOffBits;
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t biHeight;
	uint16_t biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t biClrImportant;
};

struct __attribute__(( packed )) pixel {
	unsigned char b, g, r;
};

struct image {
	uint32_t width, height;
	struct pixel* data;
};


struct pixel get_pixel( const struct image*, uint64_t, uint64_t );
void set_pixel( const struct image*, uint64_t, uint64_t, struct pixel );
uint64_t calculate_offset( uint64_t );
struct bmp_header create_bmp_header( struct image* );

#endif
