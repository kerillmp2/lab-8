#ifndef _SEPIA_H_
#define _SEPIA_H_

#include <stdlib.h>
#include "bmp_image.h"

void sepia_image( struct image*, struct image* );
void sepia_image_asm( struct image*, struct image* );
struct pixel sepia_pixel( struct pixel const pixel );
unsigned char check( uint64_t );

#endif
