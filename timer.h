#include <sys/time.h>
#include <sys/resource.h>
#include "bmp_image.h"

long set_timer( void ( *function )( struct image*, struct image* ), struct image*, struct image* );

void print_time( long time );
