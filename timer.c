#include "timer.h"

long set_timer( void ( *function ) ( struct image*, struct image* ), struct image* target, struct image* result ) {
	struct rusage r;
	struct timeval begin;
	struct timeval end;
	long time;

	getrusage( RUSAGE_SELF, &r );
	begin = r.ru_utime;
	function( target, result );
	getrusage( RUSAGE_SELF, &r );
	end = r.ru_utime;
	time = ( ( end.tv_sec - begin.tv_sec ) * 1000000L ) + end.tv_usec - begin.tv_usec;

	return time;
}

void print_time( long time ) {
	printf( "Function used %ld microseconds", time );
}
