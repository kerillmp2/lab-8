C_FLAGS = -ansi -pedantic -Wall -Werror -lm -g -c
ASM_FLAGS = -felf64 -o

main:
	gcc $(C_FLAGS) bmp_enums.c -o build/bmp_enums.o
	gcc $(C_FLAGS) bmp_io.c -o build/bmp_io.o
	gcc $(C_FLAGS) bmp_image.c -o build/bmp_image.o
	gcc $(C_FLAGS) timer.c -o build/timer.o
	nasm $(ASM_FLAGS) build/sepia_asm.o sepia_asm.asm
	gcc $(C_FLAGS) sepia.c -o build/sepia.o
	gcc $(C_FLAGS) main.c -o build/main.o
	gcc -g -lm build/*.o -o build/main

remove:
	rm -f main
