#include "main.h"

int main( void ) {

	FILE* in;
	FILE* out;
	struct image* source = ( struct image* ) malloc( sizeof( struct image ) );
	struct image* target_c = ( struct image* ) malloc( sizeof( struct image ) );
	struct image* target_asm = ( struct image *) malloc( sizeof( struct image ) );
	char* input_file = "./sq.bmp" + 0;
	char* output_file_c = "./sq_1.bmp" + 0;
	char* output_file_asm = "./sq_2.bmp" + 0;
	enum read_status read_status = ( enum read_status ) malloc( sizeof( enum read_status ) );
	enum write_status write_status = ( enum write_status ) malloc( sizeof( enum write_status ) );
	long time;

	in = fopen( input_file, "rb" );

	if( in == NULL ) {
		printf( "Can't open input file with name %s", input_file );
		return 0;
	}

	read_status = read_bmp( in, source );
	print_read_status( read_status );

	if( read_status == READ_OK ) {
		out = fopen( output_file_c, "wb" );

		if( out == NULL ) {
			printf( "Can't open output file with name %s", output_file_c );
			fclose( in );
			return 0;
		}

		time = set_timer( sepia_image, source, target_c );
		print_time( time );
		write_status = write_bmp( out, target_c );
		print_write_status( write_status );
		fclose( out );

		out = fopen( output_file_asm, "wb" );
		if( out == NULL ) {
			printf( "Can't open output file with name %s", output_file_asm );
			fclose( in );
			return 0;
		}
		time = set_timer( sepia_image_asm, source, target_asm );
		print_time( time );
		write_status = write_bmp( out, target_asm );
		print_write_status( write_status );
		fclose( out );
	}

	fclose( in );
	return 1;
}

